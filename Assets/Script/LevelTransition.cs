﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelTransition : MonoBehaviour
{

    public Animator transition;
    public float transTime = 1;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        { //TODO condition
            Fade();
        }
    }

    public void Fade()
    {

        StartCoroutine(LoadLevel(SceneManager.GetActiveScene().buildIndex + 1)); //TODO moving to different scence
    }

    IEnumerator LoadLevel(int index)
    {
        //PLay
        transition.SetTrigger("Start");
        //Wait
        yield return new WaitForSeconds(transTime);
        //Load
        SceneManager.LoadScene(index);

    }
}
